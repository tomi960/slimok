package slimoki;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
//import com.company.MenuTableFrame;

public class Slimok {
	static int rozmiar = 10;
	static int ilosc_slimakow=10;
	static int ilosc_podlewania =5;
	private static Object lock = new Object();
	public static int[][] laka = new int[rozmiar][rozmiar];
	public static int[][] laka_stara = new int[rozmiar][rozmiar];
	// ReentrantLock lock = new ReentrantLock();

	public Slimok() {
	}

	static void posadzTrawe() {
		Random generator = new Random();
		for (int i = 0; i < rozmiar; i++)
			for (int j = 0; j < rozmiar; j++)
				laka[i][j] = generator.nextInt(5);
	}

	static synchronized void pokazLake() {
		for (int i = 0; i < rozmiar; i++) {
			for (int j = 0; j < rozmiar; j++)
				System.out.print("|" + laka[i][j] + "|");
			System.out.println();
		}
		System.out.println("Nowe rozdanie.");
	}

	public static void slimakAtakuje() {
		synchronized (lock) {
			Random generator = new Random();
			int i = generator.nextInt(rozmiar);
			int j = generator.nextInt(rozmiar);
			while (laka[i][j] == 0) {
				i = generator.nextInt(rozmiar);
				j = generator.nextInt(rozmiar);
			}
			laka[i][j]--;
			laka_stara[i][j] = 1;
		}
	}

	public static void trawaRosnie() {
		synchronized (lock) {
			Random generator = new Random();
			int i = generator.nextInt(rozmiar);
			int j = generator.nextInt(rozmiar);
			while (laka[i][j] == rozmiar * rozmiar) {
				i = generator.nextInt(rozmiar);
				j = generator.nextInt(rozmiar);
			}
			laka[i][j]++;
			laka_stara[i][j] = 2;
		}
	}

	public static void main(String[] args) {

		ResourceLock lock = new ResourceLock();

		Slimok.posadzTrawe();

		// Watek a=new Watek(lock);
		//Watek2 b = new Watek2(lock);
		for (int i = 0; i < Slimok.rozmiar; i++)
			for (int j = 0; j < Slimok.rozmiar; j++)
				Slimok.laka_stara[i][i] = 0;

		Watek[] a = new Watek[ilosc_slimakow];
		Watek2[] b = new Watek2[ilosc_podlewania];
		for (int i=0; i < ilosc_slimakow; i++) {
			a[i]= new Watek();
			a[i].start();
		}
		
		for(int i=0; i<ilosc_podlewania ;i++)
		{
			b[i] = new Watek2();
			b[i].start();
		}
		//b.start();

		/*
		 * try { a.join(); b.join();
		 * 
		 * } catch (InterruptedException e) { e.printStackTrace(); }
		 */

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {

				MenuTableFrame frame = new MenuTableFrame(rozmiar, Slimok.laka, Slimok.laka_stara);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setTitle("Simple Frame");
				frame.setVisible(true);
			}
		});

	}

}

class MenuTableFrame extends JFrame {
	private static final int DEFAULT_WIDTH = 50;
	private static final int DEFAULT_HEIGHT = 50;
	private JTable table;

	public MenuTableFrame(int rozmiar, int Laka[][], int Laka_stara[][]) {
		setSize(rozmiar * DEFAULT_WIDTH, (rozmiar * DEFAULT_HEIGHT) + 65);
		table = new JTable(new ProjectTableModel(rozmiar, Laka));
		table.setRowHeight(50);
		table.setDefaultRenderer(Object.class, new ColorRenderer(Laka_stara, rozmiar));
		add(new JScrollPane(table));
	}

}

class ProjectTableModel extends AbstractTableModel {

	public int rowData[][];

	int rows;

	public ProjectTableModel(int rozmiar, int Laka[][]) {
		rows = rozmiar;
		rowData = Laka;
	}

	public Object getValueAt(int row, int col) {
		fireTableCellUpdated(row, col);
		return rowData[row][col];
	}

	public void setValueAt(int value, int row, int col) {
		rowData[row][col] = value;
	}

	public int getRowCount() {
		return rows;
	}

	public int getColumnCount() {
		return rows;
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

}

class ColorRenderer extends JLabel implements TableCellRenderer {

	public static int[][] laka_stara;

	public ColorRenderer(int[][] Laka, int rozmiar) {
		laka_stara = new int[rozmiar][rozmiar];
		laka_stara = Laka;
	}

	private static final TableCellRenderer RENDERER = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int col) {

		Component c = RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

		int wartosc = (Integer) table.getModel().getValueAt(row, col);
		int kolorek = 56280 + wartosc * 1500;
		int wartosc1 = laka_stara[row][col];
		/*
		 * if(wartosc1==1) { c.setBackground(Color.decode("16711680")); }
		 * if(wartosc1==2) {
		 */
		c.setBackground(Color.decode(String.valueOf(kolorek)));
		// }

		return c;
	}
}
